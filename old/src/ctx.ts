export function init(): [HTMLCanvasElement, CanvasRenderingContext2D] {
  const canvas = document.createElement('canvas');
  document.querySelector('#root')!.appendChild(canvas);
  const ctx = canvas.getContext('2d')!;
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;
  window.onresize = (_) => {
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
  };
  return [canvas, ctx];
}
