import './style.css';
//import { init } from './ctx';
//const [canvas, ctx] = init();

class Health {
  constructor(public health: number = 0) {}
}
class Name {
  constructor(public name: string = "") {}
}

class World implements Object {
  constructor(public components: [] = []) {}
}
const world = new World();

