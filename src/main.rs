use std::any::Any;
use std::any::TypeId;
struct World {
    entity_count: usize,
    components: Vec<(TypeId, Vec<Option<Box<dyn Any>>>)>
}
impl World {
    fn new() -> Self {
        Self { entity_count: 0, components: Vec::new() }
    }
    fn add_component<T: Any>(&mut self, component: T) -> usize {
        let id = self.entity_count;
        let type_id = TypeId::of::<T>();
        self.components.push((type_id, vec![Some(Box::new(component))]));
        id
    }
    fn get_vec_index<T: Any>(&self) -> Option<usize> {
        let type_id = TypeId::of::<T>();
        for (i, vec) in self.components.iter().enumerate() {
            if vec.0 == type_id {
                return Some(i);
            }
        }
        None
    }
}
struct Health(i8);
fn main() {
    let mut world = World::new();
    world.add_component(Health(32));
    dbg!(world.get_vec_index::<Health>());
}